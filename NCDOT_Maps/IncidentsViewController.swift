//
//  IncidentsViewController.swift
//  NCDOT_Maps
//
//  Created by John Simoneau on 8/6/15.
//  Copyright (c) 2015 John Simoneau. All rights reserved.
//

import UIKit

class IncidentsViewController: UIViewController, UITableViewDataSource, UITableViewDelegate {

    
   @IBOutlet weak var tableView: UITableView!
    
    private let incidents = [
        "Accident",
        "Another Accident",
        "Road Closed",
        "Shoulder Closed",
        "Ramp Closed",
        "Construction",
        "Maintenance"
    ]
    
    let incidentsTableIdentifier = "IncidentsTableIdentifier"
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return incidents.count
    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        var cell = tableView.dequeueReusableCellWithIdentifier(incidentsTableIdentifier) as? UITableViewCell
        if cell == nil {
            cell = UITableViewCell(style: UITableViewCellStyle.Default, reuseIdentifier: incidentsTableIdentifier)
        }
        cell!.textLabel?.text = incidents[indexPath.row]
        return cell!
    }
    
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
