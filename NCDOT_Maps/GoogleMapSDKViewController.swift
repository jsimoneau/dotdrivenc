//
//  GoogleMapSDKViewController.swift
//  NCDOT_Maps
//
//  Created by John Simoneau on 7/31/15.
//  Copyright (c) 2015 John Simoneau. All rights reserved.
//

import UIKit
import CoreLocation
import GoogleMaps

class GoogleMapSDKViewController: UIViewController, CLLocationManagerDelegate {
    
    @IBOutlet weak var loadMap: GMSMapView!
    
    var locationManager = CLLocationManager()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        locationManager.delegate = self
        locationManager.requestWhenInUseAuthorization()
    }
    
    func locationManager(manager: CLLocationManager!, didChangeAuthorizationStatus status: CLAuthorizationStatus) {
        println("didChangeAuthorizationStatus")
        switch status {
        case .NotDetermined:
            println(".NotDetermined")
            break
        case .AuthorizedWhenInUse:
            println(".AuthorizedWhenInUse")
            locationManager.startUpdatingLocation()
            break
        case .Denied:
            println(".Denied")
            break
        default:
            println("Unhandled authorization status")
            break
        }
    }
    
    func locationManager(manager: CLLocationManager!, didUpdateLocations locations: [AnyObject]!) {
        let location = locations.last as! CLLocation
        var camera = GMSCameraPosition.cameraWithLatitude(location.coordinate.latitude, longitude: location.coordinate.longitude, zoom: 12)
        locationManager.stopUpdatingLocation()
        loadMap.camera = camera
        loadMap.myLocationEnabled = true
        loadMap.trafficEnabled = true
        loadMap.settings.myLocationButton = true
        loadMap.settings.compassButton = true
        
        var position = CLLocationCoordinate2DMake(36.00, -78.00)
        var marker = GMSMarker(position: position)
        marker.title = "Hello World"
        marker.icon = GMSMarker.markerImageWithColor(UIColor.blueColor())
        marker.opacity = 0.6
        marker.map = loadMap
        
    }
    

    

    
}
